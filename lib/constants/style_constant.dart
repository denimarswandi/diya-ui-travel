import 'package:diya_ui_travel/constants/color_contant.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

var mTitleStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w600, color: mTitleColor, fontSize: 16);

var mMoreDiscountStyle = GoogleFonts.inter(
    fontSize: 12, fontWeight: FontWeight.w700, color: mBlueColor);

var mServiceTitleStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w500, fontSize: 12, color: mTitleColor);

var mServiceSubtitleStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w400, fontSize: 10, color: mSubtitleColor);

var mPopularDestinationTitleStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w700, fontSize: 16, color: mCardTitleColor);

var mPopularDestinationSubtitleStyle = GoogleFonts.inter(
    fontWeight: FontWeight.w500, fontSize: 10, color: mCardSubtitleColor);

var mTravLogTitleStyle = GoogleFonts.inter(
    fontSize: 14, fontWeight: FontWeight.w900, color: mFillColor);

var mTravLogContentStyle = GoogleFonts.inter(
    fontSize: 10, fontWeight: FontWeight.w500, color: mTitleColor);

var mTravLogPlaceStyle = GoogleFonts.inter(
    fontSize: 10, fontWeight: FontWeight.w500, color: mBlueColor);
